import React, { Component } from 'react';
import { Router, Scene, Tabs } from 'react-native-router-flux';
import { Alert, Text, TouchableOpacity, StyleSheet } from 'react-native'
import login from './App/login';
import HomeScreen from './App/HomeScreen';
import test from './App/test';
import reviewerActivity from './App/reviewerActivity';
import editorActivity from './App/editorActivity';
import splash from './App/splash';

import AsyncStorage from '@react-native-community/async-storage';
import { Actions ,ActionConst} from 'react-native-router-flux'; 
/**
 * @Router :- Declaration of whole app screens for Navigation/routing.
 */

const App = () => {
 
    function logout()
    {
      AsyncStorage.setItem('loginStatus',"false")
      Actions.login({ type: ActionConst.RESET })
    }
  function test()
    {
      Alert.alert(
        'Do you want to logout',
        '',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
         
          {text: 'Yes', onPress: () => logout()},
         
        ],
        {cancelable: false},
      );
}        
    

  return (
    <Router navigationBarStyle={{ backgroundColor: '#094FA4' ,}}tintColor='#fff' >
      <Scene key="root">
     
         <Scene key="splash"
          component={splash}
          hideNavBar={true}
          initial
        />
          
        <Scene key="login"
          component={login}
          hideNavBar={true}
        />

        <Scene key="test"
          component={test}
          hideNavBar={true}
        /> 
        <Tabs
          key="mediaContainerTabs"
          swipeEnabled
       //   hideNavBar={true}
          title=""
          titleStyle={{ color: '#fff', flex: 1, textAlign: 'center', }}
          showLabel={true}
          tabBarPosition='top'
          tabBarStyle={{ backgroundColor: "#fff" }}
          labelStyle={{ color: "black", fontSize: 11 }}
          indicatorStyle={{ backgroundColor: "#094FA4" }}
          activeBackgroundColor="white"
          wrap={false}
          onRight={ ()=> test() }
            rightButtonImage={require('./assets/images/logout.png')}
          lazy
        >
           {/* <Scene key="Scene" tabs={true} wrap={false}> */}
          <Scene
            key="HomeScreen"
            component={HomeScreen}
            title="Auther Activity"
            tintColor="red"
            back={false}
            hideNavBar={true}
          />
          <Scene
            key="reviewerActivity"
            component={reviewerActivity}
            title="Reviewer Activity"
            tintColor="red"
            back={false}
            hideNavBar={true}

          />
          <Scene
            key="editorActivity"
            component={editorActivity}
            title="Editor Activity"
            tintColor="red"
            back={false}
            hideNavBar={true}

          />

{/* 
          </Scene> */}

        </Tabs>

      </Scene>
    </Router>
  );
}

export default App;