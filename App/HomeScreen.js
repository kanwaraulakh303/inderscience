/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';

// import { WebView } from 'react-native-webview';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    FlatList,
    ScrollView
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import ChartView from 'react-native-highcharts';
// import console = require('console');

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 * 
 * 
 */
var arr = []
const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export class homeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textbox1: '',
            textbox2: '',
            abc: '',
            autherId: ''
        };

    }
    componentWillMount() {
        AsyncStorage.getItem('fldUserID')
            .then((value) => {


                this.state.autherId = value
                this.autherdata()
            })

    }
    autherdata() {


        this.setState({ spinner: true })
        fetch('http://dev.indersciencesubmissions.com/ossi/staging/session/index.php?action=authorStatsApi&userId=' + this.state.autherId,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',

                },

            })

            .then((response) => response.json())
            .then((responseData) => {

                if (responseData.length) {
                    arr = responseData
                    this.setState({ spinner: false })


                }
                else {
                    this.setState({ spinner: false })



                }
                // arr = []
                // this.setState({ enterNumber: '', })

            })
            .catch((error) => {
                this.setState({ spinner: false, error: error, enterNumber: '' })
                this.state.scancount = 1

                //this.dropDownAlertRef.alertWithType('error', 'Error', error);
                console.error(error);

            })

        // var userDetails = this.props.userData
        // for (i = 0; i < userDetails.length; i++) {
        //     if (userDetails[i].fldRoleId == '1') {
        //         arr.push(userDetails[i])

        //         break
        //     }
        // }
        // for (k = 0; k < arr.length; k++) {

        //     var dateStr = arr[k].fldDateCreated; //returned from mysql timestamp/datetime field
        //     var a = dateStr.split(" ");
        //     var d = a[0].split("-");
        //     var t = a[1].split(":");
        //     //var date = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);

        //     var da = new Date(d[0], (d[1] - 1), d[2], t[0], t[1], t[2]);
        //     var dd = da.getDate();

        //     var mm = da.getMonth() + 1;
        //     var yyyy = da.getFullYear();
        //     var today = mm + '-' + dd + '-' + yyyy;

        //     arr[k].created_date = today
        // }


    }
    componentWillUnmount() {
        arr = []

    }
    render() {

        var Highcharts = 'Highcharts';
        // Highcharts.setOptions({
        //     colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
        //    });
        var conf = {
            chart: {
                backgroundColor: '#F7F7F7',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            exporting: { enabled: false },
            title: {
                text: ''
            },

            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#094FA4',
                        '#2A70C5',
                        '#1D3B5F',
                        '#44658E',
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Easy Sharing',
                    y: 38.0,
                    sliced: true,
                    selected: true,
                }, {
                    name: 'Drag & Drop',
                    y: 9.4
                }, {
                    name: 'Beautiful Chart',
                    y: 29.0
                }, {
                    name: 'Intuitive Design',
                    y: 24.0
                },]
            }]
        };

        const options = {
            global: {
                useUTC: false
            },
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        };
        return (
            <View style={styles.container}>

                <ScrollView style={{ marginBottom: 5, }}>
                <ChartView style={{ width: WINDOW_WIDTH, height: WINDOW_HEIGHT / 2, backgroundColor: '#F7F7F7', }} config={conf} javaScriptEnabled domStorageEnabled originWhitelist={['']} options='' />
               
                    {/* <ChartView style={{ width: WINDOW_WIDTH, height: WINDOW_HEIGHT / 2, backgroundColor: '#F7F7F7', }} config={conf} options={options}></ChartView> */}

                    <View style={{ flex: 1, }}>


                        <View style={styles.centerPosition}>
                            <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 10 }]}><Text style={styles.columnTextStyle}>S.No</Text></View>
                            <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 1.5 }]}><Text style={styles.columnTextStyle}>Submission type</Text></View>
                            <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 4.5 }]}><Text style={styles.columnTextStyle}>Count</Text></View>

                        </View>



                        <View style={styles.centerPosition}>
                            <FlatList
                                data={arr}

                                renderItem={({ item, index }) =>

                                    <View style={styles.centerPosition}>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 10, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{parseInt(index) + 1}</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 1.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.name}</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 4.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.value}</Text></View>

                                    </View>

                                }
                            />
                        </View>

                    </View>

                </ScrollView>
            </View>

        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F7F7F7',
        alignItems: 'center'
    },
    centerPosition: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center'
    },
    columnStyle: {
        borderWidth: 0.5,
        borderColor: 'grey',

        height: WINDOW_HEIGHT / 28,

        backgroundColor: '#DADADA'
    },
    columnTextStyle:
    {
        fontSize: 12.5,
        margin: 5
    },
    textDesign: {
        fontWeight: 'bold',
        fontSize: 30,
        color: 'grey',
      //  fontFamily: 'Bitter-Regular'
    }
});

export default homeScreen;
