/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    FlatList,
    ScrollView
} from 'react-native';
import ChartView from 'react-native-highcharts';
// //import { Dropdown } from 'react-native-material-dropdown';
// import { WebView } from 'react-native-webview';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';


/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */
var arr = []
var arr1 = []
var graphDataArr = [
]
const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height

export class editorActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textbox1: '',
            textbox2: '',
            spinner: false,
            editorId: ''

        };

    }
    componentWillUnmount() {
        graphDataArr = [{
            name: 'Articles submitted in ',
            y: 0,
            sliced: true,
            selected: true,
        },
        {
            name: 'Articles submitted in ',
            y: 0
        }, {
            name: 'Articles submitted in ',
            y: 0
        },
        {
            name: 'Articles submitted in ',
            y: 0
        }
        ]
       
    }
    componentWillMount() {
        debugger
        AsyncStorage.getItem('fldUserID')
            .then((value) => {

                this.state.editorId = value
                this.editorData()
            })
    }

    editorData() {

        // AsyncStorage.getItem('fldUserID')
        // .then((value) => {

        //     this.state.editorId = value
        // })
        this.setState({ spinner: true })
        fetch('http://dev.indersciencesubmissions.com/ossi/staging/session/index.php?action=editorStatsApi&userId=' + this.state.editorId,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',

                },

            })

            .then((response) => response.json())
            .then((responseData) => {
                debugger
                if (responseData.length) {
                    arr = responseData
                    for (i = 0; i < responseData[0].records.length; i++) {
                        graphDataArr.push({name: 'Articles submitted in ' + responseData[0].records[i].year,y:responseData[0].records[i].articleCount})
                       
                    }
                    this.setState({ spinner: false })


                }
                else {
                    this.setState({ spinner: false })



                }
                // arr = []
                // this.setState({ enterNumber: '', })

            })
            .catch((error) => {
                this.setState({ spinner: false, error: error, enterNumber: '' })
                this.state.scancount = 1

                //this.dropDownAlertRef.alertWithType('error', 'Error', error);
                console.error(error);

            })
    }
    render() {
        var Highcharts = 'Highcharts';
        // Highcharts.setOptions({
        //     colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
        //    });
        var conf = {
            chart: {
                backgroundColor: '#F7F7F7',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            exporting: { enabled: false },
            title: {
                text: ''
            },

            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    colors: [
                        '#094FA4',
                        '#2A70C5',
                        '#1D3B5F',
                        '#44658E',
                    ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Articles',
                colorByPoint: true,
                data: graphDataArr
            }]
        };

        const options = {
            global: {
                useUTC: false
            },
            lang: {
                decimalPoint: '.',
                thousandsSep: '.'
            }
        };

        let data = [{
            value: 'Banana',
        }, {
            value: 'Mango',
        }, {
            value: 'Pear',
        }];
        return (
            <View style={styles.container}>

                {/* <Dropdown style={styles.dropdownContainer}
        label='Favorite Fruit'
        data={data}  /> */}
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <ScrollView style={{ marginBottom: 5 }}>
                <ChartView style={{ width: WINDOW_WIDTH, height: WINDOW_HEIGHT / 2, backgroundColor: '#F7F7F7', }} config={conf} javaScriptEnabled domStorageEnabled originWhitelist={['']} options='' />
              
                    {/* <ChartView style={{ width: WINDOW_WIDTH, height: WINDOW_HEIGHT / 2, backgroundColor: '#F7F7F7', }} config={conf} options={options}></ChartView>
                    */}
                    <FlatList
                        data={arr}

                        renderItem={({ item, index }) =>
                            <ScrollView horizontal={true}>
                                <View style={{ flex: 1, margin: 5 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ margin: 10, fontWeight: 'bold' }}>Journal :</Text>
                                        <Text style={{ marginTop: 10 }}>{item.journalName}</Text>
                                        <Text style={{ margin: 10 }}>-</Text>
                                        <Text style={{ margin: 10, fontWeight: 'bold' }}>Journal Manager : </Text>
                                        <Text style={{ marginTop: 10 }}>{item.journalManagerName}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', }} >

                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 8 }]}><Text style={styles.columnTextStyle}>Year</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5 }]}><Text style={styles.columnTextStyle}>Acceptance rate</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5 }]}><Text style={styles.columnTextStyle}>Throughout time</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 2.5 }]}><Text style={styles.columnTextStyle}>Total articles submitted</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 2.5 }]}><Text style={styles.columnTextStyle}>Articles accepted using review process</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 2.5 }]}><Text style={styles.columnTextStyle}>Articles accepted without review process</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5 }]}><Text style={styles.columnTextStyle}>Articles rejected</Text></View>
                                        <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5 }]}><Text style={styles.columnTextStyle}>Articles still in review</Text></View>


                                    </View>




                                    <FlatList
                                        data={item.records}

                                        renderItem={({ item, index }) =>

                                            <View style={{ flexDirection: 'row' }} >
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 8, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.year}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.acceptanceRate}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.throughputTime}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 2.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.articleCount}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 2.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.articlesAccepted}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 2.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.articleswReviewing}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.articlesRejected}</Text></View>
                                                <View style={[styles.columnStyle, { width: WINDOW_WIDTH / 3.5, backgroundColor: '#fff' }]}><Text style={styles.columnTextStyle}>{item.articlesReviewing}</Text></View>

                                            </View>

                                        }
                                    />


                                </View>
                            </ScrollView>
                        }
                    />
                </ScrollView>
            </View>

        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F7F7F7',
        alignItems: 'center'
    },
    dropdownContainer: {
        //flex: 1,
        //flexDirection: "row",
        backgroundColor: '#ff0000',
        //alignItems: 'flex-start',
        height: 150,
        width: 300,
    },
    centerPosition: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center'
    },
    columnStyle: {
        flex: 1,
        borderWidth: 0.5,
        borderColor: 'grey',
        width: WINDOW_WIDTH / 5.04,

        backgroundColor: '#DADADA'
    },
    columnTextStyle:
    {
        fontSize: 12.5,
        margin: 5
    },
    textDesign: {
        fontWeight: 'bold',
        fontSize: 30,
        color: 'grey',
       // fontFamily: 'Bitter-Regular'
    },
    spinnerTextStyle: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 25,
      //  fontFamily: 'Bitter-Regular'
    },
});

export default editorActivity;
