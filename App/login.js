/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Image,
  Button
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { Actions ,ActionConst} from 'react-native-router-flux'; // New code
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height
export function logoutFun()
{
  // AsyncStorage.setItem('reviewerID', '')
  // AsyncStorage.setItem('fldUserID', '')
  // AsyncStorage.setItem('loginStatus','')
  // this.login.setState({isModalVisible: !this.state.isModalVisible})
  
  
    return(
      <Modal isVisible={true}>
      <View style={{ flex: 1 }}>
        <Text>Hello!</Text>
        <TouchableOpacity onPress={() => {
        this.logoutFun();
      }}>
      <Text>sdsdd</Text>  
        </TouchableOpacity>
      </View>
    </Modal>
    )
  
 // this.setState({ isModalVisible: !this.state.isModalVisible });
 // Actions.login({ type: ActionConst.RESET })
}
export class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textbox1: '',
      textbox2: '',
      spinner: false,
      wrongUP: '',
      isModalVisible:false
    };
   
  }

  /**
   * @loginFun :- This function is used to login user through api with credential validations. 
   * @api :- 'https://mowbuddy-uat-api.azurewebsites.net/api/Account/login'
   */
componentWillMount()
{
  logoutFun.call()
}
   
  loginFun() {

    
    //  this.state.textbox1 = 'rahul'
    //  this.state.textbox2 = 'rahul@123'

     //Actions.mediaContainerTabs()

    if (this.state.textbox1 == "" || this.state.textbox1 == " ") {
      this.dropDownAlertRef.alertWithType('error', 'Error', "Please enter Username");
    }
    else if (this.state.textbox2 == "" || this.state.textbox2 == " ") {
      this.dropDownAlertRef.alertWithType('error', 'Error', "Please enter Password");
    }
   
    else {
      this.setState({ spinner: true })
 //editor101 dov2lac
 
           
      fetch('http://dev.indersciencesubmissions.com/ossi/staging/index.php?action=loginApi&txtUsername='+this.state.textbox1+'&txtPassword='+this.state.textbox2, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
           'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: JSON.stringify({
          "username": this.state.textbox1,
          "password": this.state.textbox2

        })
      })
        .then((response) => response.json())
        .then((responseData) => {
          
          if (responseData.status == '400') {
            this.setState({ wrongUP: responseData.message })
          }
          else {
           
            AsyncStorage.setItem('fldUserID', responseData.fldUserID)
            for(i=0;i<responseData.Roles.length;i++)
            {
              if(responseData.Roles[i].fldRole == "reviewer")
              {
              AsyncStorage.setItem('reviewerID',responseData.Roles[i].fldUserRoleID)
              }
            }

            AsyncStorage.setItem('loginStatus',"true")
            Actions.mediaContainerTabs({ type: ActionConst.RESET })
            console.log('responseData is',responseData);
          }
          this.setState({ textbox1: '', textbox2: '', spinner: false })
        })
        .catch((error) => {
          this.setState({ spinner: false })
          this.dropDownAlertRef.alertWithType('error', 'Error', error);
          console.error(error);
        })
    };
  }

  /**
   * @forgotpassFun :- This function consists of functionality of Forgot password.
   */

  forgotpassFun() {
    this.dropDownAlertRef.alertWithType('info', 'Comming soon', 'This functionality is under progress');
  }
 
  render() {

    return (
      <View source={require('../assets/images/bg_grass.jpg')} style={styles.container} >
         <Modal isVisible={this.state.isModalVisible}>
          <View style={{ flex: 1 }}>
            <Text>Hello!</Text>
            <TouchableOpacity onPress={() => {
            this.logoutFun();
          }}>
          <Text>sdsdd</Text>  
            </TouchableOpacity>
          </View>
        </Modal>
        <View>
         
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={styles.position}>
          <Image source={require('../assets/images/logo.png')} />
        </View>
        <View style={{ alignItems: 'center' }}>
          <TextInput
            style={styles.textboxDesign}
            placeholder="Username"
            placeholderTextColor="black"
            onChangeText={(textbox1) => this.setState({ textbox1, wrongUP: '' })}
            value={this.state.textbox1}
            />
          <TextInput
            style={styles.textboxDesign}
            placeholder="Password"
            placeholderTextColor="black"
            secureTextEntry={true}
            onChangeText={(textbox2) => this.setState({ textbox2, wrongUP: '' })}
            value={this.state.textbox2}
          />
          <View style={{ margin: 5, alignItems: 'center' }}>
            <Text style={{ color: 'red', fontSize: 16 }}>{this.state.wrongUP}</Text>
          </View>
          <TouchableOpacity onPress={() => {
            this.loginFun();
          }}>
            <View style={{ backgroundColor: "#094FA4", height: WINDOW_HEIGHT / 14, width: WINDOW_WIDTH / 1.5, alignItems: 'center',justifyContent:'center', marginTop: 20,borderRadius: 3.5 }}>
              <Text style={[styles.textStyle3]}>Sign In</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            this.forgotpassFun();
          }}>
          </TouchableOpacity>
        </View>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  position: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom:40
  },
  textStyle: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fff',
  },
  textStyle2: {
    fontSize: 20,
    color: '#fff',
    fontFamily: "HelveticaNeueLTStd-Lt_0",
  },
  textboxDesign: {
    width: WINDOW_WIDTH / 1.5,
    marginTop: 25,
    borderRadius: 2.5,
    fontSize: 17,
    color: 'grey',
    borderBottomWidth: 1,
  },
  textStyle3: {
    //marginTop: WINDOW_HEIGHT / 14  /4,
    color: "#fff", 
    fontSize: 22,
    fontWeight: 'bold',
    alignItems: 'center',
    // fontFamily: 'Bitter-Regular'
  },
  spinnerTextStyle: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 25,
   // fontFamily: 'Bitter-Regular'
  },
  backgroundImage: {
  }
});

export default login;



               