/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Image,

} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux'; // New code
import AsyncStorage from '@react-native-community/async-storage';

/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height


export class splash extends Component {


  constructor(props) {
    super(props);
    this.state = {
      textbox1: '',
      textbox2: '',
      spinner: false,
      wrongUP: '',
      splashValue:''

    };

  }

  componentDidMount() {
    setTimeout(() => {
     
      AsyncStorage.getItem('loginStatus')
      .then((value) => {

          
          this.state.splashValue = value
          this.splashLogic()
      })
           
          
    

    }, 2000);


  }

  splashLogic()
  {
    if(this.state.splashValue == "true")
    {
      Actions.mediaContainerTabs({ type: ActionConst.RESET })
    }
    else{
      Actions.login({ type: ActionConst.RESET })
    }
   
  }

  render() {
    return (

      <View style={styles.container} >


        <Image source={require('../assets/images/logo.png')} />



      </View>


    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default splash;
