/**
 *  React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Image
} from 'react-native';
import HomeScreen from './HomeScreen';
import DropdownAlert from 'react-native-dropdownalert';
import { Actions ,ActionConst} from 'react-native-router-flux'; // New code
import Spinner from 'react-native-loading-spinner-overlay';


/**
 * @WINDOW_Width :- Global declaration of screen width.
 * @WINDOW_HEIGHT :-   Global declaration of screen height.
 */

const WINDOW_WIDTH = Dimensions.get('window').width
const WINDOW_HEIGHT = Dimensions.get('window').height


export class test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textbox1: '',
      textbox2: '',
      spinner: false,
      wrongUP: ''
    };
    alert(this.props.userData)
  }



  render() {

    return (

     <HomeScreen></HomeScreen>


    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  position: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  textStyle: {
    fontWeight: 'bold',
    fontSize: 50,
    color: '#fff',

  },
  textStyle2: {
    fontSize: 20,
    color: '#fff',
    fontFamily: "HelveticaNeueLTStd-Lt_0",

  },
  textboxDesign: {
    width: WINDOW_WIDTH / 1.5,
    marginTop: 18,
    borderRadius: 3.5,
    fontSize: 15,
    color: 'grey',
    borderBottomWidth: 1.2,

  },
  textStyle3: {
    fontSize: 15,
    marginTop: 13,
    // fontFamily: 'Bitter-Regular'

  },
  spinnerTextStyle: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 25,
    //fontFamily: 'Bitter-Regular'
  },
  backgroundImage: {

  }
});

export default test;
